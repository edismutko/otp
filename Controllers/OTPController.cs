﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using OTP.Core.Services;

namespace OTP.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class OTPController : ControllerBase
    {
        private readonly IOTPService otp;
        private readonly IOTPTransportService transportService;

        public OTPController(IOTPService otp, IOTPTransportService transportService)
        {
            this.otp = otp;
            this.transportService = transportService;
        }
       
        [HttpGet("bysms/{number}")]
        async public Task<ActionResult> SendOTPBySMS(string number)
        {
            bool result = await Task<bool>.Run(() =>
            {
                string otp = this.otp.Generate(number);
                try
                {
                    transportService.Send(EOTPTransport.SMS, otp, number);
                    return true;
                }
                catch
                {
                    return false;
                }
            });

            if (result)
            {
                return Ok();
            }
            else
            {
                return Problem("there was a problem sending the sms.");
            }
        }

        [HttpPost("verify/{number}/{otp}")]
        async public Task<bool> VerifyOTP(string number, string otp)
        {
            return await Task<bool>.Run(()=> { return this.otp.Verify(number, otp); });
        }
    }
}
