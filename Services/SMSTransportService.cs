﻿using OTP.Core.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OTP.Services
{
    public class SMSTransportService : ITransportService
    {
        public void Send(string otp, string phoneNumbr)
        {
            Console.WriteLine("sms with otp:" + otp + " was sent to the phone number:" + phoneNumbr);
        }
    }
}
