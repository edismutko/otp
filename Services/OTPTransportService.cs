﻿using Microsoft.Extensions.Configuration;
using OTP.Core.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace OTP.Services
{
    public class OTPTransportService : IOTPTransportService
    {
        IDictionary<EOTPTransport, ITransportService> transportServices;

        public OTPTransportService()
        {
            transportServices = new Dictionary<EOTPTransport, ITransportService>();
            AddTransportService(EOTPTransport.SMS, new SMSTransportService());
        }

        public void Send(EOTPTransport transport, string otp, string endpoint)
        {
            transportServices[transport].Send(otp, endpoint);
        }

        public void AddTransportService(EOTPTransport transport, ITransportService transportService)
        {
            transportServices.Add(transport, transportService);
        }
    }
}
