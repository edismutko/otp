﻿using Microsoft.Extensions.Caching.Memory;
using OTP.Core.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace OTP.Services
{
    public class OTPService : IOTPService
    {
        TimeSpan timeToLive;
        int tokenLength;

        IMemoryCache store;

        public OTPService(IMemoryCache store)
        {
            this.store = store;
            this.timeToLive = TimeSpan.FromMinutes(5);
            this.tokenLength = 6;
        }

        public string Generate(string id)
        {
            string token = GenerateToken();
            AddTokenToStore(id, token);
            return token;
        }

        private void AddTokenToStore(string id, string token)
        {
            this.store.Set<string>(id, token, new MemoryCacheEntryOptions()
            {
                AbsoluteExpiration = DateTime.Now.AddTicks(timeToLive.Ticks),
                SlidingExpiration = timeToLive
            });
        }

        private string GenerateToken()
        {
            Random random = new Random();
            string characters = "0123456789";
            StringBuilder result = new StringBuilder(this.tokenLength);
            for (int i = 0; i < this.tokenLength; i++)
            {
                result.Append(characters[random.Next(characters.Length)]);
            }
            return result.ToString();
        }

        public bool Verify(string id, string otp)
        {
            string activeOTP;
            if (this.store.TryGetValue<string>(id, out activeOTP))
            {
                this.store.Remove(id);
                return otp == activeOTP;
            }
            return false;
        }
    }
}
