﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OTP.Core.Services
{
    public interface IOTPService
    {
        string Generate(string id);
        bool Verify(string id, string otp);
    }
}
