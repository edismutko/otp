﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OTP.Core.Services
{
    public interface ITransportService
    {
        void Send(string otp, string phoneNumbr);
    }
}
