﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OTP.Core.Services
{
    public enum EOTPTransport
    {
        SMS    
    }

    public interface IOTPTransportService
    {
        void Send(EOTPTransport transport, string otp, string phoneNumbr);
    }
}
